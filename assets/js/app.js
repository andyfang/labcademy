$(document).ready(function(){
	var $navbar = $('#header');
	var $editor = $('.editor');
	var $video = $('#video');
	var $preview = $('#preview-html');
	var $instructions = $('#instructions');
	var navbarHeight = $navbar.outerHeight();
	var playerHeight = window.innerHeight - navbarHeight;
	var previewWidth = window.innerWidth - $('#instructions').width - $('#editor-html').width;
	$editor.css({'height': playerHeight-40 + 'px'});
	$video.css({'height': playerHeight/2 + 'px'});
	$preview.css({'height': playerHeight + 'px', 'width': previewWidth + 'px'});
	$instructions.css({'height': playerHeight + 'px'});
	$('#instructions header').on('click', function(){
		$(this).toggleClass('active');
		$('ul.dropdown').toggle();
	});

	$('.course-list a').on('click', function(){
		$('.modal_container').show();
	});
	$('.bg').on('click', function(){
		$('.modal_container').hide();
	});

});
