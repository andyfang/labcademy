
function Editor(name, language){
	var self = this;
	self.language = language;
	self.editor = ace.edit("editor-" + self.language);
	self.editor.session.setMode("ace/mode/" + self.language);
	self.editor.setTheme("ace/theme/tomorrow_night");
	self.editor.setOptions({
		fontFamily: "monaco",
		fontSize: "13pt",
		enableEmmet: true,
		cursorStyle: "wide",
		wrap: true
	});

	self.editor.$blockScrolling = Infinity; // disable annoying console warnings

	self.$pop = Popcorn('#video');
	self.$vid = $('#video');
	self.$preview = $('#preview-' + self.language);

	self.code = function(){
		return(self.editor.getSession().getValue());
	};

	self.showPreviewCode = function(){
		if (self.language == 'javascript') {
			self.$preview.html('<script>' + self.code() + '</script>');
		}
		else {
			self.$preview.html(self.code());
		}
	};

	self.preview = function(atTime, toDo) {
		// Runs a function at a specific time in the video
		self.$pop.cue(atTime, function() {
			toDo();
		});
	};

	self.setCode = function(code){
		self.editor.getSession().setValue(code);
	};

	self.addCode = function(code){
		curcode = self.editor.getSession().getValue();
		newcode = curcode + code;
		self.editor.getSession().setValue(newcode);
	};

	self.getCurrentTime = function() {
		return self.$pop.currentTime();
	};

	var $newTab = jQuery('<li/>', {
		class: self.language + ' ' + name.replace('.', '_'),
		text: name
	});

	$newTab.appendTo('#tabs');

	$newTab.on('click', function() {
		$('.editor').css('display', 'none');
		$('#editor-' + self.language).css('display', 'block');
	});
}


$(document).ready(function(){

	

	$('#video').on("mousedown", function(e) {
		var clickX = e.pageX;
		var clickY = e.pageY;
		var $drag = $('#video'); // $(this) doesn't work
		var videoOffset = $drag.offset();
		var outerHeight = $drag.outerHeight();
		var outerWidth = $drag.outerWidth();
		// var windowWidth = $(window).width();
		// var windowHeight = $(window).height();

		$(window).on("mousemove", function(e) {
			var newLeft = videoOffset.left + e.pageX - clickX;
			var newTop = videoOffset.top + e.pageY - clickY;
			var currentOffset = $drag.offset();
			if (newLeft < 0) {
				newLeft = 0;
			}
			else if (currentOffset.left + outerWidth + 5 > $(window).width()) {
				newLeft = $(window).width() - outerWidth - 5;
			}

			if (newTop < 0) {
				newTop = 0;
			}
			else if (currentOffset.top + outerHeight + 5 > $(window).height()) {
				newTop = $(window).height() - outerHeight - 5;
			}
			

			$drag.offset({
				left: newLeft,
				top: newTop
			});
		}).on("mouseup", function(e) {
			$(window).unbind("mousemove");
		});	

		e.preventDefault();
	});

	previewTextData = [];
	var editorObjects = {
		'html': new Editor('index.html', 'html'),
		'css': new Editor('style.css', 'css'),
		'javascript': new Editor('app.js', 'javascript')
	};
	
	var showAllPreviewCode = function() {
		// JS will modify the HTML, so render HTML first before JS
		editorObjects['html'].showPreviewCode();
		editorObjects['css'].showPreviewCode();
		editorObjects['javascript'].showPreviewCode();
	};

	for (var language in editorObjects) {
		editorObjects[language].editor.on('input', function() {
			
			showAllPreviewCode();

		});
	}

	console.log(editorObjects);

	getPreviewJSON = function(jsonFile) {
		$.getJSON(jsonFile, function(data) {
			$.each(data.previewText, function(index, previewEvent) {
				var language = previewEvent.language;
				switch(previewEvent.action) {
					case "setCode":
						console.log("set");
						var func = function() {
							$('#tabs .' + language).click();
							editorObjects[language].setCode(previewEvent.content);
						};
						previewTextData.push({
							time: previewEvent.time,
							updateFunction: func
						});
						editorObjects[language].preview(previewEvent.time, func);
						break;
					case "addCode":
						console.log("add");
						var func = function() {
							$('#tabs .' + language).click();
							editorObjects[language].addCode(previewEvent.content);
						};
						previewTextData.push({
							time: previewEvent.time,
							updateFunction: func
						});
						editorObjects[language].preview(previewEvent.time, func);
						break;
					case undefined:
						console.error("Action not defined!");
						break;
					default:
						console.error("Unrecognized action!");
						break;
				}
			});
			console.log(previewTextData);



		});
	};
	
	

	$('#video').on('seeked', function() {
		console.log('seeked!');	
		for (var language in editorObjects) {
			editorObjects[language].setCode('');
		};
		
		var currentTime = editorObjects['html'].getCurrentTime();
		for (var i = 0; i < previewTextData.length; i++) {
			if (currentTime < previewTextData[i].time) {
				break;
			}
			previewTextData[i].updateFunction();
		};
	});


	$('#tabs .html').click(); // show the html editor by default

	$('#tabs li').on('click', function(){ // Switch .active tabs
		$('#tabs li.active').removeClass('active');
		$(this).addClass('active');
	});
});

